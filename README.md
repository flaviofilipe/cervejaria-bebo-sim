
# Pré Requesitos

-   Python v3.8
-   [venv](https://docs.python.org/3/library/venv.html)
-   PostgreSQL v12


# Instalação

## Configurando ambiente

**Criando ambiente isolado**
```
python -m venv venv
```

**Ativando ambiente**
Linux
```
source venv/bin/activate
```

Windows

```
venv/Scripts/activate
```

**instalar dependencias**
```
pip install -r requirements.txt
```

**Configuração do SGBD**

-   Crie um banco de dados no PostgreSQL. O nome do banco será utilizado logo em seguida.

-   Pode-se utilizar como Enconding UTF8, e para Collate/Ctype, pt_BR.UTF-8.

# Variáveis de ambiente

-   Copie o arquivo _.env_example_ para a raiz do projeto renomeando para **.env**;

-   Preencha com as informações do banco de dados de desenvolvimento;

```
DB_HOST=127.0.0.1
DB_ENGINE=django.db.backends.postgresql
DB_NAME=
DB_USER=
DB_PASSWORD=
DB_PORT=5432
```

## Gerar secret key

    python -c 'from django.core.management.utils import get_random_secret_key; print(get_random_secret_key())'

-   Salve a saída na variáveil `SECRET_KEY` no arquivo .env

# Criação do BD

**Execução dos migrates**

    python manage.py migrate

**Criar um novo usuário**

    python manage.py createsuperuser

# Testes

    python manage.py test

# Executar a aplicação

    python manage.py runserve

> Acesse o admin em **/admin**
