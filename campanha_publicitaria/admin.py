from django.contrib import admin
from campanha_publicitaria.models import CampanhaPublicitaria


class CampanhaPublicitariaAdmin(admin.ModelAdmin):
    search_fields = ['nome']
    filter_horizontal = ('produtos',)

admin.site.register(CampanhaPublicitaria, CampanhaPublicitariaAdmin)

