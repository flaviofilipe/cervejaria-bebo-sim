from django.apps import AppConfig


class CampanhaPublicitariaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'campanha_publicitaria'
