# Generated by Django 3.2 on 2021-04-23 00:51

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('produto', '0003_auto_20210408_0008'),
    ]

    operations = [
        migrations.CreateModel(
            name='CampanhaPublicitaria',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=200)),
                ('data_inicio', models.DateField()),
                ('data_fim', models.DateField()),
                ('pessoa_propaganda', models.CharField(max_length=255)),
                ('valor_gasto', models.FloatField(blank=True, null=True)),
                ('valor_retorno', models.FloatField(blank=True, null=True)),
                ('percentual_aumento', models.FloatField(blank=True, null=True)),
                ('produtos', models.ManyToManyField(to='produto.Produto')),
            ],
            options={
                'verbose_name_plural': 'Campanhas Publicitárias',
                'ordering': ['data_inicio'],
            },
        ),
    ]
