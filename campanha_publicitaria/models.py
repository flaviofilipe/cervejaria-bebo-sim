from django.db import models
from produto.models import Produto


class CampanhaPublicitaria(models.Model):
  nome = models.CharField(max_length=200)
  data_inicio = models.DateField()
  data_fim = models.DateField()
  pessoa_propaganda = models.CharField(max_length=255)
  valor_gasto = models.FloatField(null=True, blank=True)
  valor_retorno = models.FloatField(null=True, blank=True)
  percentual_aumento = models.FloatField(null=True, blank=True)
  produtos = models.ManyToManyField(Produto)

  class Meta:
      verbose_name_plural = "Campanhas Publicitárias"
      ordering = ['data_inicio']

  def __str__(self) -> str:
      return self.nome
