from django.contrib import admin
from clientes.models import Cliente


class ClienteAdmin(admin.ModelAdmin):
    search_fields = ['razao_social']

admin.site.register(Cliente, ClienteAdmin)
