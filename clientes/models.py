from django.db import models

class Cliente(models.Model):
  razao_social = models.CharField(max_length=200)
  endereco = models.CharField(max_length=255)
  cnpj = models.CharField(max_length=255, unique=True)
  telefone = models.CharField(max_length=255)
  pessoa_contato = models.CharField(max_length=255)

  def __str__(self) -> str:
      return self.razao_social
