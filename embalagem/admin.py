from django.contrib import admin
from embalagem.models import Embalagem


class EmbalagemAdmin(admin.ModelAdmin):
    search_fields = ['name']

admin.site.register(Embalagem, EmbalagemAdmin)
# Register your models here.
