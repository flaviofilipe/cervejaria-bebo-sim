from django.db import models

class Embalagem(models.Model):
    name = models.CharField(max_length=200, verbose_name="Nome")
    description = models.TextField(null=True, blank=True, verbose_name="Descrição")
    cost = models.FloatField(verbose_name="Custo")
    volume = models.FloatField()

    class Meta:
        verbose_name_plural = "Embalagens"

    def __str__(self) -> str:
        return self.name