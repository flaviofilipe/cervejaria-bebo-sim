from django.contrib import admin
from equipe_de_vendas.models import EquipeDeVendas


class EquipeDeVendasAdmin(admin.ModelAdmin):
    search_fields = ['nome_da_equipe']
    filter_horizontal = ('regiao_da_equipe', 'vendedor_da_equipe',)

admin.site.register(EquipeDeVendas, EquipeDeVendasAdmin)

