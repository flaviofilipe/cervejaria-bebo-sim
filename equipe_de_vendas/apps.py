from django.apps import AppConfig


class EquipeDeVendasConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'equipe_de_vendas'
