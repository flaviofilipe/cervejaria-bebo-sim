from django.db import models
from regiaoDeAtendimento.models import regiaoDeAtendimento
from vendedor.models import Vendedor


class EquipeDeVendas(models.Model):
  nome_da_equipe = models.CharField(max_length=200)
  regiao_da_equipe = models.ManyToManyField(regiaoDeAtendimento)

  vendedor_da_equipe = models.ManyToManyField(Vendedor)

  class Meta:
      verbose_name_plural = "Equipe de Vendas"

  def __str__(self) -> str:
      return self.nome_da_equipe
