from django.contrib import admin
from produto.models import Produto

class ProdutoAdmin(admin.ModelAdmin):
    search_fields = ['name']
    filter_horizontal = ('embalagem',)

admin.site.register(Produto, ProdutoAdmin)
# Register your models here.
