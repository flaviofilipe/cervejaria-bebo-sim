from django.db import models
from embalagem.models import Embalagem

class Produto(models.Model):
  name = models.CharField(max_length=200)
  quantidade = models.IntegerField(default=0)
  preço = models.FloatField()
  percentual_comissao = models.FloatField(verbose_name='Percentual de comissão', null=True, blank=True)
  formula_producao = models.TextField(verbose_name='Fórmula de produção', null=True, blank=True )
  embalagem = models.ManyToManyField(Embalagem)

  def __str__(self) -> str:
    return self.name
