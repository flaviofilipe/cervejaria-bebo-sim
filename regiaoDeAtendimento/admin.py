from django.contrib import admin
from regiaoDeAtendimento.models import regiaoDeAtendimento


class regiaoDeAtendimentoAdmin(admin.ModelAdmin):
    search_fields = ['name']

admin.site.register(regiaoDeAtendimento, regiaoDeAtendimentoAdmin)
# Register your models here.
