from django.apps import AppConfig


class RegiaodeatendimentoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'regiaoDeAtendimento'
