from django.db import models

class regiaoDeAtendimento(models.Model):
  name = models.CharField(max_length=200)

  class Meta:
    verbose_name_plural = "regiões de Atendimento"

  def __str__(self) -> str:
    return self.name