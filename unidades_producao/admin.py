from django.contrib import admin
from unidades_producao.models import UnidadeProducao

class UnidadeProducaoAdmin(admin.ModelAdmin):
    search_fields = ['nome']

admin.site.register(UnidadeProducao, UnidadeProducaoAdmin)
