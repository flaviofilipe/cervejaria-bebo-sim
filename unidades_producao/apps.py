from django.apps import AppConfig


class UnidadesProducaoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'unidades_producao'
