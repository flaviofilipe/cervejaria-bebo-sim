from django.db import models

class UnidadeProducao(models.Model):
  nome = models.CharField(max_length=200)
  endereco = models.CharField(max_length=255)
  cnpj = models.CharField(max_length=255, unique=True)
  telefone = models.CharField(max_length=255)

  class Meta:
      verbose_name_plural = "Unidades de Produção"

  def __str__(self) -> str:
      return self.nome
