from django.contrib import admin
from vendas.models import Venda


class VendaAdmin(admin.ModelAdmin):
    list_display = ["vendedor", "cliente", "created_at"]
    search_fields = ['vendedor__nome', 'cliente__razao_social', 'produto__name', 'created_at']
    filter_horizontal = ('produto',)
    raw_id_fields = ('vendedor', 'cliente')
    autocomplete_fields = ('vendedor', 'cliente')

admin.site.register(Venda, VendaAdmin)

