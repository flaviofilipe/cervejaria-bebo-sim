from django.db import models
from produto.models import Produto
from vendedor.models import Vendedor
from clientes.models import Cliente


class Venda(models.Model):
    produto = models.ManyToManyField(Produto)
    vendedor = models.ForeignKey(Vendedor, related_name = "vendedor" , on_delete=models.CASCADE)
    cliente = models.ForeignKey(Cliente, related_name = "cliente" , on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Data')
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return f'{self.vendedor} -> {self.cliente}'
