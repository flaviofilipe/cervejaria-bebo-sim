from django.contrib import admin
from vendedor.models import Vendedor


class VendedorAdmin(admin.ModelAdmin):
    search_fields = ['nome']

admin.site.register(Vendedor, VendedorAdmin)
