from django.db import models

class Vendedor(models.Model):
  nome = models.CharField(max_length=60)
  data_de_admissao = models.DateField()
  cpf = models.CharField(max_length=14, unique=True)
  carteira_de_trabalho = models.CharField(max_length=255)
  endereco_residencial = models.CharField(max_length=255)
  telefone = models.CharField(max_length=11, blank=True, null=True)
  email = models.EmailField(max_length=255)

  class Meta:
      verbose_name_plural = "Vendedores"

  def __str__(self) -> str:
      return self.nome